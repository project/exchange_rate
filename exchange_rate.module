<?php

function exchange_rate_menu() {
  $items[ 'admin/config/regional/exchange_rate' ] = array(
    'title' => 'Exchange rate settings',
    'description' => "Settings for the site's exchange rate.",
    'page callback' => 'drupal_get_form',
    'page arguments' => array( 'exchange_rate_admin_page' ),
    'access arguments' => array( 'administer site configuration' ),
    'weight' => 5,
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}


function exchange_rate_admin_page() {

  $countries_info = module_invoke_all( 'exchange_rate_country_info' );

  $form = array( );
  
  $form[ 'help' ] = array(
    '#markup' =>
    t( 'These config values must be used to store information using hook_exchange_rate_cron' ),
  );
  
  foreach ( $countries_info as $country => $country_info ) {
    $form[ $country ] = array(
      '#type' => 'fieldset',
      '#title' => t( $country_info[ 'label' ] ),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    if ( isset( $country_info[ 'currencies' ] ) ) {
      $form[ $country ][ $country . '_currencies' ] = array(
        '#type' => 'checkboxes',
        '#options' => $country_info[ 'currencies' ],
        '#default_value' => variable_get( $country . '_currencies', array( ) ),
        '#title' => t( 'Please select what currencies will be enabled for this specific country' ),
      );

      if ( isset( $country_info[ 'banks' ] ) ) {
        $form[ $country ][ $country . '_banks' ] = array(
          '#type' => 'checkboxes',
          '#options' => drupal_map_assoc( array_keys( $country_info[ 'banks' ] ) ),
          '#default_value' => variable_get( $country . '_banks', array( ) ),
          '#title' => t( 'Please select what banks will be enabled for this specific country' ),
        );
      }
    }
  }

  return system_settings_form( $form );
}

function exchange_rate_block_info() {

  $countries_info = module_invoke_all( 'exchange_rate_country_info' );

  $blocks = array( );
  foreach ( $countries_info as $country => $country_info ) {
    // This example comes from node.module.
    $blocks[ $country ] = array(
      'info' => t( 'Exchange Rate Information for ' ) . t( $country_info[ 'label' ] ),
      'cache' => DRUPAL_NO_CACHE,
    );
  }

  return $blocks;
}

function exchange_rate_block_view( $delta = '' ) {
  // This example is adapted from node.module.
  $block = array( );

  $subject = module_invoke_all( 'exchange_rate_name', $delta );

  $block[ 'subject' ] = array_shift( $subject );


  $exchange_rate_results = _exchange_rate_by_country( $delta );

  if ( isset( $exchange_rate_results ) ) {
    $vars[ 'exchange_rates' ] = $exchange_rate_results;
    $vars[ 'country' ] = $delta;
    $content = theme( 'exchange_rate_block', $vars );
  }
  else {
    $content = '';
  }

  $block[ 'content' ] = $content;

  return $block;
}

function _exchange_rate_by_country( $country ) {
  $exchanges_results = array( );

  $currencies = variable_get( $country . '_currencies', array( ) );
  $currencies = array_filter( $currencies );

  if ( count( $currencies ) ) {
    $exchanges = db_select( 'exchange_rate_historical', 'erh' )
        ->fields( 'erh', array( 'bank', 'currency', 'date', 'buy', 'sell' ) )
        ->condition( 'country', $country )
        ->condition( 'date', strtotime( date( 'Y-m-d' ) ), '>=' )
        ->condition( 'currency', $currencies )
        ->groupBy( 'erh.bank' )
        ->groupBy( 'erh.currency' )
        ->orderBy( 'erh.date' )
        ->execute();

    foreach ( $exchanges as $exchange ) {
      if ( !isset( $exchanges_results[ $exchange->currency ] ) ) {
        $exchanges_results[ $exchange->currency ] = array( );
      }
      array_push( $exchanges_results[ $exchange->currency ], $exchange );
    }
  }
  return $exchanges_results;
}

/**
 * Implementation of hook_cron
 */
function exchange_rate_cron() {
  //Request to execute the cron for each exchange rate by country
  module_invoke_all( 'exchange_rate_cron' );
}

/**
 * Implementation of hook_theme
 * @param type $existing
 * @param type $type
 * @param type $theme
 * @param type $path
 * @return type 
 */
function exchange_rate_theme( $existing, $type, $theme, $path ) {
  return array(
    'exchange_rate_block' => array(
      'variables' => array( 'exchange_rates' => NULL, 'country' => NULL )
    ),
  );
}

function theme_exchange_rate_block( $vars ) {
  $exchange_rates = $vars[ 'exchange_rates' ];
  $country = $vars[ 'country' ];
  $validate_module = module_exists( 'exchange_rate_chart' );

  $output = '';
  $header = array( t( 'Bank' ), t( 'Buy' ), t( 'Sell' ) );

  foreach ( $exchange_rates as $currency => $exchange_rate ) {
    $output .= "<strong>" . $currency . "</strong>";

    $options = array( );
    foreach ( $exchange_rate as $rate ) {
      array_push( $options, array( $rate->bank, $rate->buy, $rate->sell ) );
    }

    $output.= theme( 'table', array(
      'header' => $header,
      'rows' => $options,
      'attributes' => array( 'class' => array( 'mytable' ) )
        ) );

    if ( $validate_module ) {
      $link_to_chart = l( t( 'See chart' ), 'exchange_rate/chart/' . $country . "/" . strtolower( $currency ) );
      $output.= $link_to_chart;
    }

    // Separate tables from coutries with multi currencies
    $output .= "<br/>";
  }

  return $output;
}
